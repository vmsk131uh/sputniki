from django.urls import path

from . import views

urlpatterns = [
    path("t-api/all/", views.allt),
    path('t-api/add/', views.add_tochka),
    path('t-api/adds/', views.add_tochki),
    path('map/<path_name>/', views.show_map),
    path('map/<path_name>/supergpspass/<user_name>/', views.show_map_to_all),
    path('t-api/createsputnikuser/', views.create_sputnik_user),
    path('t-api/deleteallusersdots/', views.delete_all_users_dots),
    path('dobropozhalovat/', views.dobropozhalovat),
]

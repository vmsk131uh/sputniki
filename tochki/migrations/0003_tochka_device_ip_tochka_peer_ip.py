# Generated by Django 4.2.3 on 2023-07-20 15:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("tochki", "0002_tochka_name_of_path"),
    ]

    operations = [
        migrations.AddField(
            model_name="tochka",
            name="device_ip",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name="tochka",
            name="peer_ip",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]

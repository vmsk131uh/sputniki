from django.contrib import admin

# Register your models here.

from . import models

class TochkaAdmin(admin.ModelAdmin):
    list_display = ["user_name", "name_of_path", "indx","lat","lon","provider","smart_date_time"]
    

admin.site.register(models.Tochka, TochkaAdmin)
   

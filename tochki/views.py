from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.http import HttpRequest
from django.core import serializers
## import django.http.request
from .models import Tochka

import basicauth
from django.contrib.auth import authenticate
import json
from django.views.decorators.csrf import csrf_exempt
from operator import itemgetter, attrgetter
from django.contrib.auth.models import User,Group
from django.utils.crypto import get_random_string
from django.conf import settings
import math

def get_peers_ip_address(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def allt(request: HttpRequest):
    print(request.META.get('HTTP_AUTHORIZATION',''))
    if 'HTTP_AUTHORIZATION' in request.META:
        print(basicauth.decode(request.META['HTTP_AUTHORIZATION']))
        (usr,pswd) = basicauth.decode(request.META['HTTP_AUTHORIZATION'])
        u = authenticate(username=usr,password=pswd)
        if u is not None:
            if u.groups.filter(name = 'tochki').exists():
                return HttpResponse(
                    serializers.serialize('json',Tochka.objects.all()),content_type="application/json"
                )

    return HttpResponse('Unauthorized\n',status = 401)

@csrf_exempt
def add_tochka(request: HttpRequest):
    try:
        if 'HTTP_AUTHORIZATION' in request.META:
            print(basicauth.decode(request.META['HTTP_AUTHORIZATION']))
            (usr,pswd) = basicauth.decode(request.META['HTTP_AUTHORIZATION'])
            u = authenticate(username=usr,password=pswd)
            if u is not None:
                if u.groups.filter(name = 'tochki').exists():
                    d = json.loads(request.body)
                    t = Tochka(
                        indx=d['id'], 
                        smart_date_time = d['smartDateTime'],
                        gps_date_time = d['gpsDateTime'],
                        lat = d['lat'],
                        lon = d['lon'],
                        alt = d['altitude'],
                        vertical_accuracy = d['verticalAccuracyMeters'],
                        accuracy = d['accuracy'],
                        user_name = d['user'],
                        provider = d['provider'],
                        speed = d['speed'],
                        speed_accuracy = d['speedAccuracy'],
                        bearing = d['bearing'],
                        bearing_accuracy = d['bearingAccuracyDegrees'],
                        name_of_path = d['nameOfPath'],
                        device_ip = d['ip'],
                        peer_ip = get_peers_ip_address(request)
                    )
                    t.save()
                    return HttpResponse('{"status": "ok"}', content_type="application/json")
            else:
                return HttpResponse('{"status": "Unauthorized"}', 
                                    content_type="application/json",
                                    status = 401,
                                    )
        else: 
            return HttpResponse('{"status": "Unauthorized"}', 
                                    content_type="application/json",
                                    status = 401,
                                    )   

    except Exception as err:
        print(f" ^^^^^^ {err=}, {type(err)=} ^^^^^")
        return HttpResponse('Bad Request', status = 400)
    
    return HttpResponse('{"status": "error"}', content_type="application/json")

@csrf_exempt
def add_tochki(request: HttpRequest):
    try:
        if 'HTTP_AUTHORIZATION' in request.META:
            if settings.DEBUG:
                print(basicauth.decode(request.META['HTTP_AUTHORIZATION']))
            (usr,pswd) = basicauth.decode(request.META['HTTP_AUTHORIZATION'])
            u = authenticate(username=usr,password=pswd)
            if u is not None:
                if u.groups.filter(name = 'tochki').exists():
                    dlist = json.loads(request.body)
                    listOfTochek = []
                    for d in dlist:
                        t = Tochka(
                            indx=d['id'], 
                            smart_date_time = d['smartDateTime'],
                            gps_date_time = d['gpsDateTime'],
                            lat = d['lat'],
                            lon = d['lon'],
                            alt = d['altitude'],
                            vertical_accuracy = d['verticalAccuracyMeters'],
                            accuracy = d['accuracy'],
                            user_name = d['user'],
                            provider = d['provider'],
                            speed = d['speed'],
                            speed_accuracy = d['speedAccuracy'],
                            bearing = d['bearing'],
                            bearing_accuracy = d['bearingAccuracyDegrees'],
                            name_of_path = d['nameOfPath'],
                            device_ip = d['ip'],
                            peer_ip = get_peers_ip_address(request)
                        )
                        listOfTochek.append(t)
                    
                    savedTochki = Tochka.objects.bulk_create(listOfTochek,ignore_conflicts=True)
                    savedIdsList = [ t.indx for t in savedTochki ]
                    if(len(savedTochki)==len(dlist)):
                        if settings.DEBUG:
                            print([i['id'] for i in dlist])
                            print('{\n  "status": "ok",\n  "indxsList": ' f'{savedIdsList}' '\n}\n')
                        return HttpResponse('{\n  "status": "ok",\n  "indxsList": ' f'{savedIdsList}' '\n}\n', content_type="application/json")
                    else:
                        return HttpRequest(f'{"status": "saved {len(savedTochki)} tochek from {len(dlist)}"}')
            else:
                return HttpResponse('{"status": "Unauthorized"}', 
                                    content_type="application/json",
                                    status = 401,
                                    )
        else: 
            return HttpResponse('{"status": "Unauthorized"}', 
                                    content_type="application/json",
                                    status = 401,
                                    )   

    except Exception as err:
        print(f" ^^^^^^ {err=}, {type(err)=} ^^^^^")
        return HttpResponse('Bad Request', status = 400)
    
    return HttpResponse('{"status": "error"}', content_type="application/json")


#################################
@csrf_exempt
def delete_all_users_dots(request: HttpRequest):
    try:
        if 'HTTP_AUTHORIZATION' in request.META:
            if settings.DEBUG:
                print(basicauth.decode(request.META['HTTP_AUTHORIZATION']))
            (usr,pswd) = basicauth.decode(request.META['HTTP_AUTHORIZATION'])
            u = authenticate(username=usr,password=pswd)
            if u is not None:
                if u.groups.filter(name = 'tochki').exists():
                    d = Tochka.objects.filter(user_name=usr).delete()
                    j = json.dumps(d)
                    if settings.DEBUG:
                        print("VVVVVVV")
                        print(j)
                        print('{"status": "ok", "indxsList": ['f'{d[0]}''] }')
                    return HttpResponse('{"status": "ok", "indxsList": ['f'{d[0]}''] }', content_type="application/json")
            else:
                return HttpResponse('{"status": "Unauthorized"}', 
                                    content_type="application/json",
                                    status = 401,
                                    )
        else: 
            return HttpResponse('{"status": "Unauthorized"}', 
                                content_type="application/json",
                                status = 401,
                                )   

    except Exception as err:
        print(f" ^^^^^^ {err=}, {type(err)=} ^^^^^")
        return HttpResponse('Bad Request', status = 400)
    
    return HttpResponse('{"status": "error"}', content_type="application/json")


#################################

def show_map(request: HttpRequest, path_name):
    if 'HTTP_AUTHORIZATION' in request.META:
        (usr,pswd) = basicauth.decode(request.META['HTTP_AUTHORIZATION'])    
        u = authenticate(username=usr,password=pswd)
        if u is not None and u.groups.filter(name = 'tochki').exists():

            ##full_path = [ t for t in Tochka.objects.all().order_by("indx") if t.name_of_path == path_name and t.user_name == usr ]
            checked = None
            full_path = []
            if request.method =='POST' and request.POST.get("hideDots", "") == 'true':
                checked="checked"
                for i, t in enumerate(Tochka.objects.filter(name_of_path=path_name, user_name=usr).order_by("indx")):
                    if i == 0:
                        full_path.append(t)                        
                    elif isBetterLocation(t,full_path[len(full_path)-1]):
                        full_path.append(t)
            else:
                full_path = Tochka.objects.filter(name_of_path=path_name, user_name=usr).order_by("indx")

            gpspoints = [ t for t in full_path if t.provider == "gps" ]
            networkpoints = [ t for t in full_path if t.provider == "network" ]
            fusedpoints = [ t for t in full_path if t.provider == "fused" ]
            try:
                lastpoint = full_path[len(full_path)-1]
            except (IndexError,ValueError) as error:
                print(f"^^^show_map_to_all^^^ {error=}, {type(error)=} ^^^^^")
                lastpoint = Tochka(lat=55.6, lon = 37.7)
            
            p = Tochka.objects.last()
            return render(request, "show_map.html", {
                "full_path": full_path,
                "gpspoints": gpspoints,
                "networkpoints": networkpoints,
                "fusedpoints": fusedpoints,
                "lastpoint" : lastpoint,
                "checked" : checked,
            })
        else:
            return HttpResponse(status = 401)
        
        
    else:
        return HttpResponse("Unauthorized",status = 401,headers={"WWW-Authenticate": "Basic"})

TWO_MINUTES = 60 * 2
def isBetterLocation(location: Tochka, currentBestLocation: Tochka) -> bool:
    if (currentBestLocation == None): 
        # A new location is always better than no location
        return True
    
    # Check whether the new location fix is newer or older
    timeDelta = (location.gps_date_time - currentBestLocation.gps_date_time).total_seconds()
    isSignificantlyNewer = timeDelta > TWO_MINUTES
    isSignificantlyOlder = timeDelta < -TWO_MINUTES
    isNewer = timeDelta > 0

    # If it's been more than two minutes since the current location, use the new location
    # because the user has likely moved
    if isSignificantlyNewer: 
        return True
    # If the new location is more than two minutes older, it must be worse
    elif isSignificantlyOlder:
       return False
    

    # Check whether the new location fix is more or less accurate
    accuracyDelta = math.trunc(location.accuracy - currentBestLocation.accuracy)
    isLessAccurate = accuracyDelta > 0
    isMoreAccurate = accuracyDelta < 0
    isSignificantlyLessAccurate = accuracyDelta > 200

    # Check if the old and new location are from the same provider
    isFromSameProvider = location.provider == currentBestLocation.provider

    # Determine location quality using a combination of timeliness and accuracy
    if isMoreAccurate:
        return True
    elif isNewer and not isLessAccurate:
        return True
    elif isNewer and (not isSignificantlyLessAccurate) and isFromSameProvider:
        return True
    
    return False





def show_map_to_all(request: HttpRequest, path_name, user_name):
    ##if 'HTTP_AUTHORIZATION' in request.META:
    ##    (usr,pswd) = basicauth.decode(request.META['HTTP_AUTHORIZATION'])    
    ##    u = authenticate(username=usr,password=pswd)
    ##    if u is not None and u.groups.filter(name = 'tochki').exists():

            ##full_path = [ t for t in Tochka.objects.all().order_by("indx") if t.name_of_path == path_name and t.user_name == user_name ]        
            checked = None
            full_path = []
            if request.method =='POST' and request.POST.get("hideDots", "") == 'true':
                checked="checked"
                for i, t in enumerate(Tochka.objects.filter(name_of_path=path_name, user_name=user_name).order_by("indx")):
                    if i == 0:
                        full_path.append(t)                        
                    elif isBetterLocation(t,full_path[len(full_path)-1]):
                        full_path.append(t)
            else:
                full_path = Tochka.objects.filter(name_of_path=path_name, user_name=user_name).order_by("indx")
                
            gpspoints = [ t for t in full_path if t.provider == "gps" ]
            networkpoints = [ t for t in full_path if t.provider == "network" ]
            fusedpoints = [ t for t in full_path if t.provider == "fused" ]
            try:
                lastpoint = full_path[len(full_path)-1]
            except (IndexError,ValueError) as error:
                print(f"^^^show_map_to_all^^^ {error=}, {type(error)=} ^^^^^")
                lastpoint = Tochka(lat=55.6, lon = 37.7)
            
            p = Tochka.objects.last()
            return render(request, "show_map.html", {
                "full_path": full_path,
                "gpspoints": gpspoints,
                "networkpoints": networkpoints,
                "fusedpoints": fusedpoints,
                "lastpoint" : lastpoint,
                "checked" : checked,
            })
        ##else:
        ##    return HttpResponse(status = 401)
        
        
    ##else:
    ##    return HttpResponse("Unauthorized",status = 401,headers={"WWW-Authenticate": "Basic"})

@csrf_exempt
def create_sputnik_user(request: HttpRequest):
    try:
        b = json.loads(request.body)
        u=b['login']
        if User.objects.filter(username=u).exists():
            return HttpResponse('{"status": "user exists"}\n',  status = 400,content_type="application/json")
        else:
            RANDOM_STRING_CHARSS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*{{}}=+-_,."'
            p=get_random_string(22,allowed_chars=RANDOM_STRING_CHARSS)
            print(p)
            newu=User.objects.create_user(username=u,password=p)
            tgr = Group.objects.get(name = 'tochki')
            newu.groups.add(tgr)
            newu.save()
            print(newu)
            return HttpResponse('{\n  "status": "ok",\n  "password": "'f'{p}''"\n}\n', content_type="application/json")

    except Exception as err:
        
        print(f" ^^^^^^ {err=}, {type(err)=} ^^^^^")
        return HttpResponse('{"status": "bad request"}\n', status = 400, content_type="application/json")
    
    return HttpResponse('{"status": "error"}', content_type="application/json", status = 400)


def dobropozhalovat(request: HttpRequest):
    return render(request,"dobropozhalovat.html")
from django.db import models
from django.db.models import UniqueConstraint
# Create your models here.

class Tochka(models.Model):
    indx = models.IntegerField()
    smart_date_time = models.DateTimeField()
    gps_date_time = models.DateTimeField()

    lat = models.FloatField()
    lon = models.FloatField()
    alt = models.FloatField(null=True, blank=True)
    vertical_accuracy = models.FloatField(null=True, blank=True)
    accuracy = models.FloatField(null=True, blank=True)

    user_name = models.CharField(max_length=255)
    provider = models.CharField(max_length=255)
    
    speed = models.FloatField(null=True, blank=True)
    speed_accuracy = models.FloatField(null=True, blank=True)

    bearing = models.FloatField(null=True, blank=True)
    bearing_accuracy = models.FloatField(null=True, blank=True)
    
    name_of_path = models.CharField(max_length=255, null=True, blank=True)

    device_ip = models.CharField(max_length=255, null=True, blank=True)
    peer_ip = models.CharField(max_length=255, null=True, blank=True)
    class Meta:
        verbose_name_plural = "Tochki"
        constraints = [ 
            UniqueConstraint(fields=['indx', 'user_name','gps_date_time'], name='unique_booking'),
        ]

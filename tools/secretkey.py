
### echo "$(python3 -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())')" > .env.secret_key.txt

from django.core.management.utils import get_random_secret_key

with open(".env.secret_key.txt","w") as f:
    f.write(get_random_secret_key())
    